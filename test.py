from random import SystemRandom
import socket
import threading
import unittest
import random
from client import connect_and_send_all, connect_and_send, get_server_filename, grep_command
from typing import List, Tuple, Optional, Callable
import rstr
import argparse

SERVER_LIST = [
    ("fa23-cs425-7601.cs.illinois.edu", 8005, 1),
    ("fa23-cs425-7602.cs.illinois.edu", 8005, 2),
    ("fa23-cs425-7603.cs.illinois.edu", 8005, 3),
    ("fa23-cs425-7604.cs.illinois.edu", 8005, 4),
    ("fa23-cs425-7605.cs.illinois.edu", 8005, 5),
    ("fa23-cs425-7606.cs.illinois.edu", 8005, 6),
    ("fa23-cs425-7607.cs.illinois.edu", 8005, 7),
    ("fa23-cs425-7608.cs.illinois.edu", 8005, 8),
    ("fa23-cs425-7609.cs.illinois.edu", 8005, 9),
    ("fa23-cs425-7610.cs.illinois.edu", 8005, 10)
]

# LOCAL DEBUG SERVER LIST
LOCAL_SERVER_LIST = [
    ("localhost", 8001, 1),
    ("localhost", 8002, 2),
    ("localhost", 8003, 3),
    ("localhost", 8004, 4),
    ("localhost", 8005, 5),
    ("localhost", 8006, 6),
    ("localhost", 8007, 7),
    ("localhost", 8008, 8),
    ("localhost", 8009, 9),
    ("localhost", 8010, 10)
]

FILE_NAME = "./logs/test/test.log"

rs = rstr.Rstr(SystemRandom())

def generate_log_with_pattern(
    count : int,
    appear_count : int,
    pattern : str,
    include_gen : Optional[Callable[[List[str]], str]] = None,
    exclude_gen : Optional[Callable[[List[str]], str]] = None
) -> str:
    """
    Generate a random log file, where the count is the number of lines that match the pattern.

    Args:
        count (int): The number of random lines to be generated
        appear_count (int): The number of occurrences of the pattern
        patterns (List[str]): The patterns that appears in appear_count lines

    Returns:
        str: The log file
    """
    return "\n".join(generate_list_with_pattern(count, appear_count, pattern, include_gen, exclude_gen))

def generate_list_with_pattern(
    count : int,
    appear_count : int,
    pattern : str,
    include_gen : Optional[Callable[[List[str]], str]] = None,
    exclude_gen : Optional[Callable[[List[str]], str]] = None
) -> List[str]:
    """
    Generate a random list of strings, where the count is the number of lines that match the pattern.

    Args:
        count (int): The number of random lines to be generated
        appear_count (int): The number of occurrences of the pattern
        patterns (List[str]): The patterns that appears in appear_count lines

    Returns:
        List[str]: The list of strings
    """
    include_gen = include_gen if include_gen is not None else lambda pats: rstr.letters(100, include=[pats])
    exclude_gen = exclude_gen if exclude_gen is not None else lambda pats: rstr.letters(100, exclude=pats)

    log_list = []
    for _ in range(appear_count):
        log_list.append(include_gen(pattern))
    for _ in range(count - appear_count):
        log_list.append(exclude_gen(pattern))
    # shuffle the log list
    random.shuffle(log_list)
    return log_list

def write_log_file(
    server_list : List[Tuple[str, int, int]],
    logs : List[str], filename : str
) -> bool:
    """
    Write the log file to all the servers.

    Args:
        server_list (List[Tuple[str, int, int]]): The list of servers to write to.
        logs (List[str]): The list of logs to write.
        filename (str): The filename to write to.

    Returns:
        bool: _description_
    """

    return connect_and_send_all(server_list, [{
        "operation": "write",
        "filename": get_server_filename(filename, server_number),
        "data": logs[index]
    } for index, (_, _, server_number) in enumerate(server_list)])

def test_on_server(
    server_list : List[Tuple[str, int, int]],
    command : str, log_files : List[str],
    expected_output : dict
) -> bool:
    """
    Test the grep command on all servers.

    Args:
        server_list (List[Tuple[str, int, int]]): The list of servers to test on.
        command (str): The grep command to run.
        log_files (List[str]): The list of log files to test to.
        expected_output (dict): The expected output. (server_number: expected_output)

    Returns:
        bool: Whether the test passed or not.
    """
    # write the log files to the servers
    write_result_list = write_log_file(server_list, log_files, FILE_NAME)
    test_server_list = [] # only test servers that have been written to

    print(f"Tried to run on {len(write_result_list)} servers")

    for index, write_result in enumerate(write_result_list):
        if write_result["success"]:
            test_server_list.append(server_list[index])
        else:
            server_host, server_port, server_number = server_list[index]
            print(f"Server {server_number} @ {server_host}:{server_port} is offline")

    print(f"Test on {len(test_server_list)} servers")

    # run the grep command on the servers
    server_result = grep_command(test_server_list, command, FILE_NAME, -1)

    print(f"Expected output: {expected_output}")

    server_output = {}
    for index, result in enumerate(server_result):
        _, _, server_number = test_server_list[index]
        server_output[server_number] = result["result"]

    print(f"Server output: {server_output}")

    # check if the output is correct
    for index, result in enumerate(server_result):
        _, _, server_number = test_server_list[index]
        if expected_output[server_number] != result["result"]:
            print(f"Server {server_number} failed")
            return False

    return True

def generate_log_file_and_expected_result(
    server_list : List[Tuple[str, int, int]],
    patterns : List[str],
    occurrence_counts : List[int],
    total_counts : List[int],
    include_gen : Optional[Callable[[List[str]], str]] = None,
    exclude_gen : Optional[Callable[[List[str]], str]] = None
) -> Tuple[List[str], dict]:
    """
    Generate log file and expected grep result, given the patterns, their occurrence count,
    and total log line count in each log file

    Args:
        server_list (List[Tuple[str, int, int]]): The basic information about the servers
        patterns (List[str]): The patterns that will appear in log file
        occurrence_counts (List[int]): The occurrence count for the patterns in each file
        total_counts (List[int]): The total log count in each file

    Returns:
        Tuple[List[str], dict]: The generated log file and the expected grep result
    """
    log_files = []
    expected_result = {}
    for index, pattern in enumerate(patterns):
        _, _, server_number = server_list[index]
        log_files.append(
            generate_log_with_pattern(
                total_counts[index], occurrence_counts[index], pattern,
                include_gen, exclude_gen
            )
        )
        expected_result[server_number] = occurrence_counts[index]
    return log_files, expected_result

def test_random_occurrence_on_server(
    server_list : List[Tuple[str, int, int]],
    max_line_occurrence_count : int,
    max_server_occurrence_count : int,
) -> Tuple[str, bool]:
    """
    Test grep on patterns that appear randomly on multiple lines on multiple servers.

    Args:
        server_list (List[Tuple[str, int, int]]): The basic information about the servers
        max_line_occurrence_count (int): The maximum number of lines that will have the pattern
        max_server_occurrence_count (int): The maximum number of servers that will have the pattern

    Returns:
        List[Tuple[str, bool]]: The list of test results for each pattern
    """

    occurred_server_count = random.randint(1, max_server_occurrence_count)
    occurred_server_list = random.sample(range(len(server_list)), occurred_server_count)

    return test_occurrence_on_server(
        server_list, [
            random.randint(1, max_line_occurrence_count)
            if index in occurred_server_list else 0
            for index in range(len(server_list))
        ], [200] * len(server_list)
    )

def test_occurrence_on_server(
    server_list : List[Tuple[str, int, int]],
    line_occurrence_count : List[int],
    total_line_count : List[int],
    pattern : Optional[str] = None,
    include_gen : Optional[Callable[[List[str]], str]] = None,
    exclude_gen : Optional[Callable[[List[str]], str]] = None,
    regular_expression : bool = False,
    command : Optional[str] = None,
) -> Tuple[str, bool]:
    """
    Test grep on patterns that appear on multiple lines on multiple servers.

    Args:
        server_list (List[Tuple[str, int, int]]): The basic information about the servers
        line_occurrence_count (int): The number of lines that will have the pattern
        server_occurrence_count (int): The number of servers that will have the pattern
        total_line_count (List[int]): The total log count in each file

    Returns:
        List[Tuple[str, bool]]: The list of test results for each pattern
    """


    # pattern
    pattern = rstr.letters(10) if pattern is None else pattern

    # occurrence count
    print(f"Pattern: {pattern}")
    print(f"Occurrence count list: {line_occurrence_count}")

    log_files, expected_output = generate_log_file_and_expected_result(
        server_list,
        [pattern] * len(server_list),
        line_occurrence_count,
        total_line_count,
        include_gen,
        exclude_gen
    )

    flags = "-c -E" if regular_expression else "-c"

    command = f"grep {flags} {pattern}" if command is None else command

    return pattern, test_on_server(server_list, command, log_files, expected_output)

class TestGrep(unittest.TestCase):
    """
    Test grep with different inputs.

    Args:
        unittest: the unittest class
    """
    def test_empty_file_grep(self):
        """
        Test grep on all servers with an empty file.
        """
        print("\n\n##### Test empty file #####\n\n")
        log_files = [""] * 10
        expected_output = {server_number : 0 for _, _, server_number in CURRENT_SERVER_LIST}
        self.assertTrue(test_on_server(CURRENT_SERVER_LIST, "grep -c test", log_files, expected_output), "incorrect output")

    def test_single_occurrence_on_single_file(self):
        """
        Test grep on all servers with a single occurrence of the pattern in a single file.
        """
        print("\n\n##### Test single occurrence on single file #####\n\n")

        pattern, is_success = test_random_occurrence_on_server(CURRENT_SERVER_LIST, 1, 1)

        self.assertTrue(is_success, f"incorrect output for pattern {pattern}")

    def test_single_line_multiple_server(self):
        """
        Test grep on patterns that appear on a single line on multiple servers.
        """
        print("\n\n##### Test single line multiple server #####\n\n")

        pattern, is_success = test_random_occurrence_on_server(CURRENT_SERVER_LIST, 1, len(CURRENT_SERVER_LIST))

        self.assertTrue(is_success, f"incorrect output for pattern {pattern}")
    def test_multiple_line_single_server(self):
        """
        Test grep on patterns that appear on multiple lines on a single server.
        """
        print("\n\n##### Test multiple line single server #####\n\n")
        pattern, is_success = test_random_occurrence_on_server(CURRENT_SERVER_LIST, 10, 1)

        self.assertTrue(is_success, f"incorrect output for pattern {pattern}")

    def test_multiple_line_multiple_server(self):
        """
        Test grep on patterns that appear on multiple lines on multiple servers.
        """
        print("\n\n##### Test multiple line multiple server #####\n\n")
        pattern, is_success = test_random_occurrence_on_server(CURRENT_SERVER_LIST, 10, len(CURRENT_SERVER_LIST))

        self.assertTrue(is_success, f"incorrect output for pattern {pattern}")

    def test_rare_occurrence(self):
        """
        Test grep on patterns that appear rarely.
        """
        print("\n\n##### Test rare occurrence #####\n\n")
        pattern, is_success = test_occurrence_on_server(
            CURRENT_SERVER_LIST,
            [1] * len(CURRENT_SERVER_LIST),
            [1000] * len(CURRENT_SERVER_LIST)
        )
        self.assertTrue(is_success, f"incorrect output for pattern {pattern}")

    def test_somewhat_frequent_occurrence(self):
        """
        Test grep on patterns that appear somewhat frequently.
        """
        print("\n\n##### Test somewhat frequent occurrence #####\n\n")
        pattern, is_success = test_occurrence_on_server(
            CURRENT_SERVER_LIST,
            [10] * len(CURRENT_SERVER_LIST),
            [100] * len(CURRENT_SERVER_LIST)
        )
        self.assertTrue(is_success, f"incorrect output for pattern {pattern}")

    def test_frequent_occurrence(self):
        """
        Test grep on patterns that appear frequently.
        """
        print("\n\n##### Test frequent occurrence #####\n\n")
        pattern, is_success = test_occurrence_on_server(
            CURRENT_SERVER_LIST,
            [100] * len(CURRENT_SERVER_LIST),
            [1000] * len(CURRENT_SERVER_LIST)
        )
        self.assertTrue(is_success, f"incorrect output for pattern {pattern}")

    def test_very_frequent_occurrence(self):
        """
        Test grep on patterns that appear very frequently.
        """
        print("\n\n##### Test very frequent occurrence #####\n\n")
        pattern, is_success = test_occurrence_on_server(
            CURRENT_SERVER_LIST,
            [500] * len(CURRENT_SERVER_LIST),
            [1000] * len(CURRENT_SERVER_LIST)
        )
        self.assertTrue(is_success, f"incorrect output for pattern {pattern}")

    def test_every_line_occurrence(self):
        """
        Test grep on patterns that appear on every line.
        """
        print("\n\n##### Test every line occurrence #####\n\n")
        pattern, is_success = test_occurrence_on_server(
            CURRENT_SERVER_LIST,
            [1000] * len(CURRENT_SERVER_LIST),
            [1000] * len(CURRENT_SERVER_LIST)
        )
        self.assertTrue(is_success, f"incorrect output for pattern {pattern}")

    def test_special_characters(self):
        """
        Test grep on patterns that contain special characters.
        """
        print("\n\n##### Test special characters #####\n\n")

        patterns = ["\t", "\\", "$", "{", "}"]
        commands = [
            "grep -c '\t'",
            "grep -c '\\\\'",
            "grep -c '\\$'",
            "grep -c '{'",
            "grep -c '}'"
        ]

        for index, pattern in enumerate(patterns):
            _, is_success = test_occurrence_on_server(
                CURRENT_SERVER_LIST,
                [100] * len(CURRENT_SERVER_LIST),
                [1000] * len(CURRENT_SERVER_LIST),
                pattern,
                command = commands[index]
            )
            self.assertTrue(is_success, f"incorrect output for pattern {pattern}")

    def test_regular_expression(self):
        """
        Test grep on regular expressions.
        """
        print("\n\n##### Test regular expression #####\n\n")

        pattern, is_success = test_occurrence_on_server(
            CURRENT_SERVER_LIST,
            [10] * len(CURRENT_SERVER_LIST),
            [100] * len(CURRENT_SERVER_LIST),
            "[a-z]",
            lambda pats: rstr.xeger(r'[a-z]{100}'),
            lambda pats: rstr.xeger(r'[^a-z]{100}'),
            regular_expression=True
        )
        self.assertTrue(is_success, f"incorrect output for pattern {pattern}")

        # pattern, is_success = test_occurrence_on_server(
        #     CURRENT_SERVER_LIST,
        #     [10] * len(CURRENT_SERVER_LIST),
        #     [100] * len(CURRENT_SERVER_LIST),
        #     "GET|POST",
        #     lambda pats: rstr.xeger(r'(GET|POST)'),
        #     lambda pats: rstr.xeger(r'^(?!GET$|POST$).*'),
        #     regular_expression=True
        # )
        # self.assertTrue(is_success, f"incorrect output for pattern {pattern}")

    def test_fix_and_random_lines(self):
        """
        Test grep on patterns that appear on a fixed number of lines and random number of lines.
        """
        print("\n\n##### Test fix and random lines #####\n\n")

        def include_gen(count):
            return ["I DO INCLUDE GET"] * count

        def exclude_gen(count):
            return ["I DO NOT INCLUDE <CANNOT SAY HERE>"] * count

        self.assertTrue(test_on_server(
            CURRENT_SERVER_LIST,
            "grep -c GET", [
                "\n".join(generate_list_with_pattern(500, 100, "GET") + include_gen(100) + exclude_gen(400))
                for _ in CURRENT_SERVER_LIST
            ], {
                server_number : 200 for _, _, server_number in CURRENT_SERVER_LIST
            }
        ))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Performance test for the grep command.')
    parser.add_argument('--server', action='store_true', help='Run on the server.')
    args = parser.parse_args()

    CURRENT_SERVER_LIST = SERVER_LIST if args.server else LOCAL_SERVER_LIST

    unittest.main()