# cs425mp1

Running xxx.py with --help to see the usage (if you are still unsure)

## START SERVER

```sh
    python3 server.py --server
```

## START CLIENT

```sh
    python3 client.py --server --command <command> --filename <filename>
```

Example

Don't include filename in the command
the filename, say if it's "./logs/vm.log", the client will try to access
"./logs/vm1.log", "./logs/vm2.log", "./logs/vm3.log", etc. or each different server

```sh
    python3 client.py --server --command "grep -c GET" --filename ./logs/vm.log
```

## START TEST-CASE

Make sure package "rstr" is installed

```sh
    python3 test.py --server
```

## START PERF

```sh
    python3 perf.py --server
```

## BATCH_START

Only for local test. Start all servers locally

```sh
    python3 batch_start.py
```